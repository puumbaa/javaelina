package ru.kpfu.itis.lesson2;

import java.util.ArrayList;
import java.util.List;

public class GenericsDemo {
    public static void main(String[] args) {
        List rawList = new ArrayList();
        rawList.add("123");
        rawList.add("124");
        rawList.add(124);

        for (Object o: rawList) {
            String s = (String) o;
            System.out.println(s);
        }

        List<String> genericList = new ArrayList<>();
        genericList.add("123");

        Integer intNumber = 5;
        Number number = intNumber;

        Integer[] integers = {1,2,3};
        Number[] numbers = integers; // Ковариантность (когда можем подсунуть наследника, отношением наследования сохраняется)

        List<Integer> integerList = new ArrayList<>();
        integerList.add(1);
        integerList.add(2);
        integerList.add(3);
//        List<Number> numbersList = integerList; <- ERROR - это называется Инвариантность (Типы должны совпадать точь-в-точь)

        sum(integerList);

//        sum(1,2,3,4,5,6);

        int x = 4;
        Integer wrapper = x; //  '= x' => '= new Integer(x)' <- это называется авто-упаковка (autoboxing)


    }

    public int unwrap(Integer wrapper) {
        return wrapper; // unboxing | 'return wrapper' => 'return wrapper.intValue()'
    }


    public static void test(List<?> list) {
        Object o = list.get(0);
        String s = "asd";
//        list.add(s); <- ERROR
    }

    public static void sum(int b, int... a){
        int i = a[0];
        int length = a.length;
    }


    public static int sum(List<? extends Number> numbers){  // <? extends ..> - Ковариантным (тоже самое поведение как у массивов)
        int sum = 0;
        for (Number number : numbers) {
            sum += number.intValue();
        }
        return sum;
    }

    public static void fillList(List<? super Integer> destination, Integer... integers){ // <? super ..> - Контрвариантность
        for (Integer integer : integers) {
            destination.add(integer);
        }
        Object o = destination.get(0);
    }




    public <T> void test(T t){
        String s = "";
        System.out.println(s.getClass());
//        System.out.println(T.class);
    }
}
