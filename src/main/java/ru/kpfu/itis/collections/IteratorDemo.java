package ru.kpfu.itis.collections;

import java.util.*;

public class IteratorDemo {
    public static void main(String[] args) {
        List<Integer> list = new LinkedList<>();
        list.add(1);
        list.add(2);
        list.add(3);

        Iterator<Integer> iterator = list.iterator();
        while (iterator.hasNext()){
            System.out.println(iterator.next());
        }

        MyIterable myIterable = new MyIterable();

        for (Integer i : myIterable){
            System.out.println(i);
        }

        Queue<Integer> priorityQueue = new PriorityQueue<>();
    }
    static class MyIterable implements Iterable<Integer> {

        @Override
        public Iterator<Integer> iterator() {
            return null;
        }
    }
}
