package ru.kpfu.itis.collections;

import java.util.*;

public class CollectionsDemo {
//  если o1.equals(o2) == true => o1.hashCode() должен быть равен o2.hashCode
//  если o1.equals(o2) == false => o1.hashCode() могут совпадать o2.hashCode (коллизия)
// если хешкоды сопадают, то это еще не факт что эти объекты будут равны по equals()
    public static void main(String[] args) {
//        System.out.println(simpleHashFunction("asdasd"));
//        System.out.println(simpleHashFunction("asdasd"));
//
//        Map<String, Integer> map = new HashMap<>();
//        map.put("1", 1230);

        Person person = new Person("Вася");
        Set<Person> people = new HashSet<>();
        people.add(person);
//        Map<Person, String> map = new TreeMap<>(
//                new Comparator<Person>() {
//                    @Override
//                    public int compare(Person o1, Person o2) {
//                        return 0;
//                    }
//                }
//        );
//
//        System.out.println(map.keySet());

        person.setName("Андрей");
        System.out.println("person in set? " + people.contains(person));

        Person personFromSet = people.iterator().next();

        System.out.println("are the same objects ? " + (personFromSet == person)); // true
        System.out.println("hashs are equals ? " + (personFromSet.hashCode() == person.hashCode())); // true
        System.out.println("persons are equals ? " + personFromSet.equals(person)); // true

    }




    // * => int
    public static int simpleHashFunction(String s){
        return s.length() % 100;
    }



    static class Person{
        private String name;

        public Person(String name) {
            this.name = name;
        }

        public String getName() {
            return name;
        }


        @Override
        public String toString() {
            return "Person{" +
                    "name='" + name + '\'' +
                    '}';
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;
            Person person = (Person) o;
            return Objects.equals(name, person.name);
        }

        @Override
        public int hashCode() {
            return Objects.hash(name);
        }

        public void setName(String name) {
            this.name = name;
        }
    }
}
