package ru.kpfu.itis.lesson3.functions;


import java.util.function.BinaryOperator;


public class Solution {
    public static void main(String[] args) {
        calculate(4, 3, (integer, integer2) -> integer & integer2);
    }

    static int calculate(int a, int b, BinaryOperator<Integer> operator){
        return operator.apply(a,b);
    }
}
