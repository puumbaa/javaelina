package ru.kpfu.itis.lesson3;

public class Main {
    public static void main(String[] args) {
        Worker worker1 = new StrongWorker();
        Worker worker2 = new WeakWorker();
        worker1.work();
        System.out.println();
        worker2.work();
    }
}
