package ru.kpfu.itis.lesson3;

public interface Worker {

    void rest(); // отдых

    default void work(){
        System.out.println("Hard work!!!");
        rest();
        System.out.println("Hard work!");
    }
}
