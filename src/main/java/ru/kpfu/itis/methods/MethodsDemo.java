package ru.kpfu.itis.methods;

import java.util.ArrayList;
import java.util.List;

public class MethodsDemo {
    public static void main(String[] args) {
        List<Integer> integers = new ArrayList<>();
        integers.add(1);
        integers.add(2);
        integers.add(3);
        test(integers);
        System.out.println(integers);


    }

    public static void test(List<Integer> list){
        list.add(4);
        list = new ArrayList<>();
    }
}
