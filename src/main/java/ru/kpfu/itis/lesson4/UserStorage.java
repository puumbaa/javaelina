package ru.kpfu.itis.lesson4;

import java.util.Map;
import java.util.Random;

public class UserStorage {
    private Map<Long, User> data;

    public User getById(Long id) {
        return data.get(id);
    }

    public void save(User user) {
        long id = new Random().nextLong();
        user.setId(id);
        data.put(id, user);
    }

}
