package ru.kpfu.itis.lesson4;

import lombok.Data;

@Data
public class SignUpForm {
    private String name;
    private String surname;
    private int age;
}
