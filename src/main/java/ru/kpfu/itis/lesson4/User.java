package ru.kpfu.itis.lesson4;

import lombok.AllArgsConstructor;
import lombok.Data;


@Data
@AllArgsConstructor
public class User {
    private Long id;
    private String name;
    private String surname;
    private int age;
}
