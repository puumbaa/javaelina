package ru.kpfu.itis.lesson4;

import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public class UserService {
    private final UserStorage userStorage; // userStorage - внешняя зависимость, мы хотим изолироваться от нее

    public User getUser(Long id) {
        return userStorage.getById(id);
    }

    public void addUser(SignUpForm form) {
        User user = new User(
                null,
                form.getName(),
                form.getSurname(),
                form.getAge()
        );
        userStorage.save(user);
    }
}
