package ru.kpfu.itis.homeworks;

import java.util.Stack;

public class BracketsSeq {
    public static void main(String[] args) {
        String br = "asdw[dw(das)adw]";
        boolean answer = new BracketsSeq().BScorrectOrNo(br);
        System.out.println(answer);
    }

    public boolean BScorrectOrNo(String s){
        if (s == null) {
            return false;
        }
        Stack<Character> brackets = new Stack<>();
        for(int i = 0; i < s.length(); i++){
            if(s.charAt(i) == '(' | s.charAt(i) == '[' | s.charAt(i) == '{'){
                brackets.push(s.charAt(i));
            }
        }

        for(int i = 0; i < s.length(); i++){
            if(s.charAt(i) == ')' && brackets.peek() == '('
                    || (s.charAt(i) == ']' && brackets.peek() == '[') || s.charAt(i)== '}' && brackets.peek() == '{'){
                brackets.pop();
            }
        }
        return brackets.isEmpty();
    }

}
