package ru.kpfu.itis.lesson4;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import static org.junit.jupiter.api.Assertions.*;


class UserServiceTest {
    private UserService userService;
    @Mock
    private UserStorage userStorage;
    private AutoCloseable autoCloseable;

    private static final User USER_BY_ID = new User(1L, "name", "surname", 10);

    @BeforeEach
    void setUp() {
        autoCloseable = MockitoAnnotations.openMocks(this);
        Mockito.when(userStorage.getById(1L)).thenReturn(USER_BY_ID);
        userService = new UserService(userStorage);
    }

    @AfterEach
    void tearDown() {
        try {
            autoCloseable.close();
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    @Test
    void getUser() {
        User actual = userService.getUser(1L);
        Mockito.verify(userStorage, Mockito.times(1)).getById(1L);
        Assertions.assertEquals(USER_BY_ID, actual);
    }

    @Test
    void addUser() {
        // todo: write test
    }
}