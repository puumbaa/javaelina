package ru.kpfu.itis.homeworks;

import org.junit.jupiter.api.*;


/**
 * 1) BeforeAll
 * 2)
 **/
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
public class BracketsSeqTest {
 // 1 test: this = 1426, bracketsSeq = 1427
//  2 test: this = 1440  bracketsSeq = 1441
    private BracketsSeq bracketsSeq;

    @BeforeAll
    static void beforeAll() {

    }

    @BeforeEach
    void init() {
        bracketsSeq = new BracketsSeq();
    }

    @Test
    void returnsTrueWhenCorrectInputString() {
        boolean actual = bracketsSeq.BScorrectOrNo("asdw[dw(das)adw]");
        Assertions.assertTrue(actual);
    }

    @Test
    void returnsFalseWhenNull() {
        boolean actual = bracketsSeq.BScorrectOrNo(null);
        Assertions.assertFalse(actual);
    }

    @AfterEach
    void tearDown() {
        // do actions after test
    }

    @AfterAll
    void afterAll() {

    }
}
